// George Gomez
// 1755012
// pa3
// List.c

#include<stdio.h>
#include<stdlib.h>
#include "List.h"

// struct is same as public class Node in java
typedef struct NodeObj
{
    int data;
    struct NodeObj* prev;
    struct NodeObj* next;
}NodeObj;

typedef NodeObj* Node; // private Node type

// struct is same as public class List in java
typedef struct ListObj
{
    Node front;
    Node back;
    Node cursor;
    int index;
    int length;
}ListObj;

// newNode()
// same as Node class constructor
Node newNode(int data)
{
    Node N = NULL;
    N = malloc(sizeof(NodeObj));
    //printf("List.c: newNode(): N malloc address: %p\n", N);
    N->data = data;
    N->prev = NULL;
    N->next = NULL;
    return (N);
}


void freeNode(Node* pN)
{
   
    if (pN != NULL && *pN != NULL)
    {
        //printf("!!List.c: freeNode(): free(*pN) called\n");
        free(*pN);
        *pN = NULL;
    }
}


List newList(void)
{
    List L = NULL;
    L = malloc(sizeof(ListObj));
    L->front = NULL;
    L->back = NULL;
    L->cursor = NULL;
    L->index = -1;
    L->length = 0;
    return (L);
}

void freeList(List* pL)
{
    //printf("List.c: freeList() called\n");
    if (pL != NULL && *pL!= NULL)
    {
        while (length(*pL) > 0)
        {
            
            deleteFront(*pL);
        }
       
        free(*pL);
        *pL = NULL;
    }
}


int length(List L)
{
    if (L == NULL)
    {
        printf("List Error: calling length() on NULL List reference\n");
        exit(1);
    }
    return L->length;
}

int index(List L)
{
  =
    if (L == NULL)
    {
        printf("List Error: calling index() on NULL List reference\n");
        exit(1);
    }
    if (length(L) <= 0)
    {
   
        return -1;
    }
  
    return L->index;
}


int front(List L)
{
    if (L == NULL)
    {
        printf("List Error: calling front() on NULL List reference\n");
        exit(1);
    }
    if (length(L) <= 0)
    {
        printf("List Error: front() called on empty List\n");
    }
    return L->front->data;
}


int back(List L)
{
    if (L == NULL)
    {
        printf("List Error: calling back() on NULL List reference\n");
        exit(1);
    }
    if (length(L) <= 0)
    {
        printf("List Error: back() called on empty List\n");
    }
    return L->back->data;
}


int get(List L)
{
    if (L == NULL)
    {
        printf("List Error: calling get() on NULL List reference\n");
        exit(1);
    }
    if (length(L) <= 0)
    {
        printf("List Error: get() called on empty List\n");
    }
    if (L->cursor == NULL)
    {
        printf("List Error: cannot get() if cursor is NULL\n");
    }
    return L->cursor->data;
}

int equals(List A, List B)
{
    int eq = 0;
    Node M = NULL;
    Node N = NULL;
    if (A == NULL || B == NULL)
    {
        printf("List Error: calling equals() on NULL List reference\n");
        exit(1);
    }
    eq = (A->length == B->length);
    M = A->front;
    N = B->front;
    while (eq && M != NULL)
    {
        eq = (M->data == N->data);
        M = M->next;
        N = N->next;
    }
    return eq;
}


void clear(List L)
{
    if (L == NULL)
    {
        printf("List Error: calling clear() on a NULL List reference\n");
        exit(1);
    }
    while (length(L) > 0)
    {
        
        deleteFront(L);
    }
    L->front = NULL;
    L->back = NULL;
    L->cursor = NULL;
    L->index = -1;
    L->length = 0;
}

void moveFront(List L)
{
    
    if (L == NULL)
    {
        printf("List Error: calling moveFront() on a NULL List reference\n");
        exit(1);
    }
    if (length(L) > 0)
    {
        L->cursor = L->front;
        L->index = 0;
    }
}

void moveBack(List L)
{
    if (L == NULL)
    {
        printf("List Error: calling moveBack() on a NULL List reference\n");
        exit(1);
    }
    if (length(L) > 0)
    {
        L->cursor = L->back;
        L->index = L->length - 1;
    }
}

void movePrev(List L)
{
    if (L == NULL)
    {
        printf("List Error: calling movePrev() on a NULL List reference\n");
        exit(1);
    }
    if (L->cursor == L->front)
    {
        L->cursor = NULL;
        L->index = -1;
    }
    else
    {
        L->cursor = L->cursor->prev;
        L->index--;
    }
}

void moveNext(List L)
{
    if (L == NULL)
    {
        printf("List Error: calling moveNext() on a NULL List reference\n");
        exit(1);
    }
    if (L->cursor == L->back)
    {
        L->cursor = NULL;
        L->index = -1;
    }
    else
    {
        L->cursor = L->cursor->next;
        L->index++;
    }
}

void prepend(List L, int data)
{
    if (L == NULL)
    {
        printf("List Error: calling prepend() on a NULL List reference\n");
        exit(1);
    }
    Node N = newNode(data);
    if (L->front == NULL)
    {
        L->front = N;
        L->back = N;
        L->cursor = L->front;
    }
    else
    {
        L->front->prev = N;
        N->next = L->front;
        L->front = N;
        L->index++;
    }
    L->length++;
}

{
    if (L == NULL)
    {
        printf("List Error: calling apend() on a NULL List reference\n");
        exit(1);
    }
    Node N = newNode(data);
    if (L->back == NULL)
    {
        L->front = N;
        L->back = N;
        L->cursor = L->back;
    }
    else
    {
        L->back->next = N;
        N->prev = L->back;
        L->back = N;
        N->next = NULL;
    }
    L->length++;
}

void insertBefore(List L, int data)
{
    if (L == NULL)
    {
        printf("List Error: calling apend() on a NULL List reference\n");
        exit(1);
    }
    if (L->cursor == NULL)
    {
        printf("List Error: cannot insertBefore()  if cursor is undefined\n");
    }
    if (L->cursor == L->front)
    {
        prepend(L, data);
    }
    else
    {
        Node N = newNode(data);
        N->prev = L->cursor->prev;
        N->next = L->cursor;
        L->cursor->prev->next = N;
        L->cursor->prev = N;
        L->index++;
        L->length++;
    }
}

void insertAfter(List L, int data)
{
    if (L == NULL)
    {
        printf("List Error: calling apend() on a NULL List reference\n");
        exit(1);
    }
    if (length(L) <= 0)
    {
        printf("List Error: insertAfter() called on empty List\n");
    }
    if (L->cursor == L->back)
    {
        append(L, data);
    }
    else
    {
        Node N = newNode(data);
        L->cursor->next->prev = N;
        N->next = L->cursor->next;
        N->prev = L->cursor;
        L->cursor->next = N;
        L->length++;
    }
}

void deleteFront(List L)
{
    //printf("List.c: deleteFront() called\n");
    if (L == NULL)
    {
        printf("List Error: calling apend() on a NULL List reference\n");
        exit(1);
    }
    if (length(L) <= 0)
    {
        printf("List Error: cannot deleteFront() empty List\n");
    }
    else
    {
        

      if (L->length == 1)
      {
          Node N = L->front;

          freeNode(&N);
         
          
          L->cursor = NULL;
          L->front = L->back = NULL;
          L->index = -1;
      }
      else //if (L->length > 1)
      {
          Node N = L->front;
          
          L->front = L->front->next;
          L->front->prev = NULL;
          if (L->cursor != NULL)
          {
              L->index--;
          }
          freeNode(&N);
      }
      L->length--;
  }
}


void deleteBack(List L)
{
    if (L == NULL)
    {
        printf("List Error: cannot deleteBack() on NULL List reference\n");
    }
    if (length(L) <= 0)
    {
        printf("List Error: cannot deleteBack() empty List\n");
    }
    else
    {
        if (L->length == 1)
        {
            Node N = L->back;
            freeNode(&N);
            L->cursor = NULL;
            L->front = L->back = NULL;
            L->index = -1;
        }
        else 
        {
            Node N = L->back;
            L->back = L->back->prev;
            L->back->next = NULL;
            if (L->index == L->length - 1)
            {
                L->index = -1;
            }
            freeNode(&N);
        }
        L->length--;
        
    }
}



void delete(List L)
{
    if (L == NULL)
    {
        printf("List Error: cannot deleteBack() on NULL List reference\n");
    }
    if (length(L) <= 0)
    {
        printf("List Error: cannot delete() empty List");
    }
    if (L->cursor == NULL)
    {
        printf("List Error: cannot delete() on undefined cursor");
    }
    if (L->cursor == L->front)
    {
        deleteFront(L);
    }
    else if (L->cursor == L->back)
    {
        deleteBack(L);
    }
    else
    {
        Node N = L->cursor;
        L->cursor->prev->next = L->cursor->next;
        L->cursor->next->prev = L->cursor->prev;
        freeNode(&N);
        L->length--;
    }
    L->index = -1;
}


void printList(FILE* out, List L)
{
    Node N = NULL;
    if(L == NULL)
    {
        printf("List error: printList() called on NULL List reference\n");
        exit(1);
    }
    for (N = L->front; N != NULL; N = N->next)
    {
        fprintf(out, "%d", N->data);
        fprintf(out, " ");
    }
}


List copyList(List L)
{
    List C = newList();
    if (length(L) > 0)
    {
        Node N = L->front;
        while (N != NULL)
        {
            append(C, N->data);
            N = N->next;
        }
    }
    C->cursor = NULL;
    C->index = -1;
    return C;
}