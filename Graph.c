// George Gomez
// 1755012
// pa3
// Graph.c: Graph ADT

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"Graph.h"

#define WHITE 0
#define GRAY 1
#define BLACK 2

typedef struct GraphObj
{
    List* neighbors;
    int* color;
    int* parent;
    int* discover;
    int* finish;
    int order; // number of vertices
    int size; // number of edges
}GraphObj;

typedef struct GraphObj* Graph;

Graph newGraph(int n)
{
    Graph G = malloc(sizeof(GraphObj));
    G->neighbors = calloc(n + 1, sizeof(List));
    G->color = calloc(n + 1, sizeof(int));
    G->parent = calloc(n + 1, sizeof(int));
    G->discover = calloc(n + 1, sizeof(int));
    G->finish = calloc(n + 1, sizeof(int));    
    
    G->order = n;
    G->size = 0;
    for (int i = 1; i <= n; i++)
    {
        G->neighbors[i] = newList();
        G->color[i] = WHITE;
        G->parent[i] = NIL;
        G->discover[i] = UNDEF;
        G->finish[i] = UNDEF;
    }
    return G;
}


void freeGraph(Graph* pG)
{
    Graph ptr = *pG;
   
    for(int i = 1; i <= getOrder(ptr); i++)
    {
        freeList(&(ptr->neighbors[i]));
    }

    // free rest and set *pG to NULL
    free(ptr->neighbors);
    free(ptr->color);
    free(ptr->parent);
    free(ptr->discover);
    free(ptr->finish);

   
    free(*pG);
    *pG = NULL;
}


int getOrder(Graph G)
{
    if (G == NULL)
    {
        printf("Graph error: cannot getOrder() on NULL Graph reference\n");
        exit(1);
    }
    return G->order;
}

int getSize(Graph G)
{
    if (G == NULL)
    {
        printf("Graph error: cannot getSize() on NULL graph reference\n");
        exit(1);
    }
    return G->size;
}

int getParent(Graph G, int u)
{
    if (G == NULL)
    {
        printf("Graph error: cannot getSource() on NULL Graph reference\n");
        exit(1);
    }
    if (!(1 <= u && u <= getOrder(G)))
    {
        printf("Graph error: cannot getParent() if u is out of bounds\n");
        exit(1);
    }
    return G->parent[u];
}

int getDiscover(Graph G, int u)
{
    if (G == NULL)
    {
        printf("Graph error: cannot getDiscover() on NULL Graph reference\n");
        exit(1);
    }
    if (!(1 <= u && u <= getOrder(G)))
    {
        printf("Graph error: cannot getParent() if u is out of bounds\n");
        exit(1);
    }
    return G->discover[u];
}

int getFinish(Graph G, int u)
{
    if (G == NULL)
    {
        printf("Graph error: cannot getFinish() on NULL Graph reference\n");
        exit(1);
    }
    if (!(1 <= u && u <= getOrder(G)))
    {
        printf("Graph error: cannot getParent() if u is out of bounds\n");
        exit(1);
    }
    return G->finish[u];
}

void addSortedEdge(List L, int v)
{
    //printf("in addSortedEdge(): v is: %d\n", v);
    if (L == NULL)
    {
        printf("Graph error: cannot addSortedEdge() on NULL List reference\n");
        exit(1);
    }
    if (length(L) == 0)
    {
        append(L, v);
        return;
    }    
    moveFront(L);
    while (index(L) != -1)
    {
        if (v < get(L))
        {
            insertBefore(L, v);
            break;
        }
        moveNext(L);
    }
    if (index(L) == -1)
    {
        append(L, v);
    }
}

void addEdge(Graph G, int u, int v)
{
    //printf("in addEdge(): u is: % d, v is: %d\n", u, v);
    if (G == NULL)
    {
        printf("Graph error: cannot addEdge() on NULL Graph reference\n");
        exit(1);
    }
    if (!(1 <= u && u <= getOrder(G)))
    {
        printf("Graph error: cannot getPath() if u is out of bounds\n");
        exit(1);
    }
    if (!(1 <= v && v <= getOrder(G)))
    {
        printf("Graph error: cannot getPath() if v is out of bounds\n");
        exit(1);
    }
    addSortedEdge(G->neighbors[u], v);
    addSortedEdge(G->neighbors[v], u);
    G->size++;
}

void addArc(Graph G, int u, int v)
{
    if (G == NULL)
    {
        printf("Graph error: cannot addArc() on NULL Graph reference\n");
        exit(1);
    }
    if (!(1 <= u && u <= getOrder(G)))
    {
        printf("Graph error: cannot getPath() if u is out of bounds\n");
        exit(1);
    }
    if (!(1 <= v && v <= getOrder(G)))
    {
        printf("Graph error: cannot getPath() if v is out of bounds\n");
        exit(1);
    }
    addSortedEdge(G->neighbors[u], v);
    G->size++;
}

void DFS(Graph G, List S) 
{
    if (G == NULL)
    {
        printf("Graph error: cannot DFS() on NULL Graph reference\n");
        exit(1);
    }
    if (S == NULL)
    {
        printf("Graph error: cannot DFS on NULL List reference\n");
        exit(1);
    }
    if (length(S) != getOrder(G)) 
    {
        printf("Graph error: cannot DFS() if length(S) != getOrder(G)\n");
        exit(1);
    }
    for (int i = 1; i <= getOrder(G); i++) 
    {
        G->color[i] = WHITE;
        G->parent[i] = NIL;
    }
    int time = 0;
    moveFront(S);
    while (index(S) >= 0) 
    {
        int u = get(S);
        //printf("in DFS(): u is: %d\n", u);
        if (G->color[u] == WHITE) 
        {
            visit(G, S, u, &time);
        }
        moveNext(S);
    }
    for (int i = 0; i < getOrder(G); i++) 
    {
        deleteBack(S);
    }
}

void visit(Graph G, List S, int u, int *time) 
{
    if (G == NULL)
    {
        printf("Graph error: cannot DFS() on NULL Graph reference\n");
        exit(1);
    }
    if (S == NULL)
    {
        printf("Graph error: cannot DFS on NULL List reference\n");
        exit(1);
    }
    G->discover[u] = ++*time;
    G->color[u] = GRAY;
    moveFront(G->neighbors[u]);
    while (index(G->neighbors[u]) >= 0) 
    {
        int y = get(G->neighbors[u]);
        
        if (G->color[y] == WHITE) 
        {
            G->parent[y] = u;
           
            visit(G, S, y, time);
        }
        moveNext(G->neighbors[u]);
    }
    G->color[u] = BLACK;
    G->finish[u] = ++*time;
    prepend(S, u);
}

Graph transpose(Graph G) 
{
    Graph T = newGraph(getOrder(G));
    for (int i = 1; i <= getOrder(G); i++) 
    {
        moveFront(G->neighbors[i]);
        if (length(G->neighbors[i]) == 0) 
        {
            continue;
        }
        else 
        {
            while (index(G->neighbors[i]) != -1) 
            {
                addArc(T, get(G->neighbors[i]), i);
                moveNext(G->neighbors[i]);
            }
        }
    }
    return T;
}

Graph copyGraph(Graph G) 
{
    Graph C = newGraph(getOrder(G));
    for (int i = 1; i <= getOrder(G); i++) 
    {
        moveFront(G->neighbors[i]);
        if (length(G->neighbors[i]) == 0)
        {
            continue;
        }
        else
        {
            while (index(G->neighbors[i]) >= 0) 
            {
                addArc(C, i, get(G->neighbors[i]));
                moveNext(G->neighbors[i]);
            }
        }
    }
    return C;
}

void printGraph(FILE* out, Graph G)
{
    if (G == NULL)
    {
        printf("Graph error: printGraph() called on NULL Graph reference\n");
        exit(1);
    }
    for (int i = 1; i <= getOrder(G); i++)
    {
        
        List list = G->neighbors[i];
        fprintf(out, "%d:", i);
        moveFront(list);
        
        while (index(list) != -1)
        {
            fprintf(out, " %d", get(list));
            moveNext(list);
        }
        fprintf(out, "\n");
    }
}