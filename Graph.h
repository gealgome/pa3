// George Gomez
// 1755012
// pa3
// Graph.h

#ifndef _GRAPH_H_INCLUDE_
#define _GRAPH_H_INCLUDE_
#define NIL 0
#define UNDEF -1
#include "List.h"

typedef struct GraphObj* Graph; // exported type



Graph newGraph(int n);


void freeGraph(Graph* pG);

int getOrder(Graph G);


int getSize(Graph G);


int getParent(Graph G, int u);


int getDiscover(Graph G, int u);


int getFinish(Graph G, int u);


void addArc(Graph G, int u, int v);


void addEdge(Graph G, int u, int v);


void DFS(Graph G, List S);


void visit(Graph G, List S, int u, int *time);

Graph transpose(Graph G);


Graph copyGraph(Graph G);


void printGraph(FILE* out, Graph G);

#endif